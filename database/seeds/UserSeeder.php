<?php

use App\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Max I',
            'email' => 'axeseco@mail.ru',
            'password' => 'fkg7hv'
        ]);

        $trainer = User::create([
            'name' => 'Trainer',
            'email' => 'trainer@trainer.ru',
            'password' => 'password'
        ]);

        $trainer->addRole(2);
    }
}
