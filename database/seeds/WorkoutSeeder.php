<?php
/**
 * Created by PhpStorm.
 * User: max
 * Date: 20.09.16
 * Time: 12:22
 */

use App\Workout;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class WorkoutSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $workout = Workout::create([
            'name' => 'Second workout',
            'description' => 'Back and biceps training',
            'start' => Carbon::createFromTime(20, 0, 0),
            'user_id' => 1,
            'duration' => '2:00:00',
        ]);

        $workout->exercises()->attach(1);
        $workout->exercises()->attach(2);
        $workout->exercises()->attach(3);

        $workout = Workout::create([
            'name' => 'Chest',
            'description' => 'Chest and triceps',
            'start' => Carbon::createFromTime(20, 0, 0),
            'user_id' => 1,
            'duration' => '2:00:00',
        ]);

        $workout->exercises()->attach(1);
        $workout->exercises()->attach(2);
        $workout->exercises()->attach(3);
    }
}