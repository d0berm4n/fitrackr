<?php

use App\Unit;
use Illuminate\Database\Seeder;

class UnitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Unit::create([
            'name' => 'шт'
        ]);

        Unit::create([
            'name' => 'кг'
        ]);

        Unit::create([
            'name' => 'м'
        ]);

        Unit::create([
            'name' => 'мин'
        ]);
    }
}
