<?php

use App\Category;
use App\Exercise;
use App\Metric;
use Illuminate\Database\Seeder;

class ExerciseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Metric::create([
            'name' => 'Подходы',
            'unit_id' => 1
        ]);

        Metric::create([
            'name' => 'Повторения',
            'unit_id' => 1
        ]);

        Metric::create([
            'name' => 'Вес',
            'unit_id' => 2
        ]);

        $exercise = Exercise::create([
            'name' => 'Жим лежа'
        ]);

        $exercise->metrics()->attach(1);
        $exercise->metrics()->attach(2);
        $exercise->metrics()->attach(3);

        $exercise = Exercise::create([
            'name' => 'Становая'
        ]);

        $exercise->metrics()->attach(1);
        $exercise->metrics()->attach(2);
        $exercise->metrics()->attach(3);

        $exercise = Exercise::create([
            'name' => 'Приседания'
        ]);

        $exercise->metrics()->attach(1);
        $exercise->metrics()->attach(2);
        $exercise->metrics()->attach(3);

        Category::create([
            'name' => 'Для начинающих'
        ]);

        Category::create([
            'name' => 'Для опытных'
        ]);

        Category::create([
            'name' => 'Для профессиональных спортсменов'
        ]);
    }
}
