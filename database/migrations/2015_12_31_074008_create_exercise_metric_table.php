<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExerciseMetricTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exercise_metric', function (Blueprint $table) {
            $table->integer('exercise_id')->unsigned();
            $table->integer('metric_id')->unsigned();
            $table->foreign('exercise_id')->references('id')->on('exercises')->onDelete('cascade');
            $table->foreign('metric_id')->references('id')->on('metrics')->onDelete('cascade');
            $table->primary(['exercise_id', 'metric_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('exercise_metric', function (Blueprint $table) {
            $table->dropForeign('exercise_metric_metric_id_foreign');
            $table->dropForeign('exercise_metric_exercise_id_foreign');
        });

        Schema::drop('exercise_metric');
    }
}
