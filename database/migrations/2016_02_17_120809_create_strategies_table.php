<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStrategiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('strategies', function (Blueprint $table) {
            $table->integer('workout_template_id')->unsigned();
            $table->foreign('workout_template_id')->references('id')->on('workout_templates')->onDelete('cascade');
            $table->integer('exercise_id')->unsigned();
            $table->foreign('exercise_id')->references('id')->on('exercises')->onDelete('cascade');
            $table->integer('metric_id')->unsigned();
            $table->foreign('metric_id')->references('id')->on('metrics')->onDelete('cascade');

            $table->text('value');

            $table->boolean('enabled');
            $table->enum('direction', ['up', 'down'])->nullable();
            $table->float('difference')->nullable();
            $table->enum('units', ['percent', 'unit'])->nullable();
            $table->enum('period', ['month', 'week'])->nullable();

            $table->timestamps();
            $table->primary(['workout_template_id', 'exercise_id', 'metric_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('strategies');
    }
}
