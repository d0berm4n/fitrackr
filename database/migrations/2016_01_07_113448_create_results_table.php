<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('results', function (Blueprint $table) {
            $table->integer('workout_id')->unsigned();
            $table->foreign('workout_id')->references('id')->on('workouts')->onDelete('cascade');
            $table->integer('exercise_id')->unsigned();
            $table->foreign('exercise_id')->references('id')->on('exercises')->onDelete('cascade');
            $table->integer('metric_id')->unsigned();
            $table->foreign('metric_id')->references('id')->on('metrics')->onDelete('cascade');
            $table->text('value');
            $table->primary(['workout_id', 'exercise_id', 'metric_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('results');
    }
}
