<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExerciseWorkoutTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exercisables', function (Blueprint $table) {
            $table->integer('exercisable_id')->unsigned();
            $table->integer('exercise_id')->unsigned();
            $table->string('exercisable_type');
            $table->foreign('exercise_id')->references('id')->on('exercises')->onDelete('cascade');
            $table->primary(['exercisable_id', 'exercise_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('exercisables');
    }
}
