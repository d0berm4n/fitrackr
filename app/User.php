<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Request;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt($value);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function workouts()
    {
        return $this->hasMany('App\Workout');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function clients()
    {
        return $this->belongsToMany('App\User', 'client_user', 'user_id', 'client_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function staff()
    {
        return $this->belongsToMany('App\User', 'client_user', 'client_id', 'user_id');
    }

    /**
     *Get user's roles
     */
    public function roles()
    {
        return $this->belongsToMany('App\Role');
    }

    /**
     * @return bool
     */
    public function isAdmin()
    {
        return $this->hasRole('admin');
    }

    /**
     * @return bool
     */
    public function isStaff()
    {
        return $this->hasRole('staff');
    }

    /**
     * @param $rolename
     * @return bool
     */
    protected function hasRole($rolename)
    {
        return $this->roles()->where('name', $rolename)->count() > 0;
    }

    /**
     * @param $roleID
     */
    public function addRole($roleID)
    {
        $this->roles()->attach($roleID);
    }

    public function scopeAutocomplete($query)
    {
        return $query->where('name', 'like', '%' . request()->input('query') . '%');
    }

    public function programs()
    {
        return $this->hasMany('App\TrainingProgram');
    }
}
