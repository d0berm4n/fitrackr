<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Support\Facades\Auth;

class WorkoutRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if(Auth::user()->isStaff())
        {
            return [
                'user_id' => 'required',
                'name' => 'required',
                'exercises' => 'required',
            ];
        }
        else
        {
            return [
                'name' => 'required',
                'exercises' => 'required',
            ];
        }
    }

}
