<?php

namespace App\Http\Controllers;

use App\WorkoutTemplate;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class WorkoutTemplateController extends Controller
{

    function __construct()
    {
        $this->middleware('auth');
        $this->middleware('staff');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $template = WorkoutTemplate::create($request->all());

        foreach($request->input('exercises') as $exercise)
        {
            $template->exercises()->attach($exercise['id']);

            foreach($exercise['metrics'] as $metric)
            {
                $template->strategies()->create([
                    'metric_id' => $metric['id'],
                    'exercise_id' => $exercise['id'],
                    'value' => $metric['value'],
                    'enabled' => $metric['enabled'] == 'true',
                    'direction' => $metric['direction'],
                    'difference' => $metric['difference'],
                    'units' => $metric['units'],
                    'period' => $metric['period']
                ]);
            }
        }

        $template->load('exercises.strategies');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        WorkoutTemplate::findOrFail($id)->delete();
    }
}
