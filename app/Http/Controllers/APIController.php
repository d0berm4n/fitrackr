<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use Hash;
use Illuminate\Support\Facades\Auth;
use JWTAuth;

class APIController extends Controller
{
    public function register(Request $request)
    {
        $input = $request->all();
        $input['password'] = Hash::make($input['password']);
        User::create($input);
        return response()->json(['result'=>true]);
    }

    public function login(Request $request)
    {
        $input = $request->all();

        if (!$token = JWTAuth::attempt($input))
        {
            return response()->json([
                'ok' => false,
                'error' => 'wrong email or password.',
            ]);
        }

        return response()->json([
            'ok' => true,
            'token' => $token,
            'user' => Auth::user()->toArray()
        ]);
    }

    public function get_user_details(Request $request)
    {
        $input = $request->all();
        $user = JWTAuth::toUser($input['token']);
        return response()->json(['result' => $user]);
    }
}
