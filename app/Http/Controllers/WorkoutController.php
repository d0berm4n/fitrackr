<?php

namespace App\Http\Controllers;

use App\Http\Requests\WorkoutRequest;
use App\Workout;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class WorkoutController extends Controller
{
//    function __construct()
//    {
//        $this->middleware('auth');
//    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(Workout::allWithExercises());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(WorkoutRequest $request)
    {
        $fields = $request->all();

        if (!Auth::user()->isStaff())
        {
            $fields['user_id'] = Auth::user()->id;
        }

        $workout = Workout::create($fields);

        foreach($request->input('exercises') as $exercise)
        {
            $workout->exercises()->attach($exercise['id']);

            foreach($exercise['metrics'] as $metric)
            {
                $workout->results()->create([
                    'metric_id' => $metric['id'],
                    'exercise_id' => $exercise['id'],
                    'value' => $metric['value']
                ]);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $workout = Workout::findOrFail($id);

        if(Auth::user()->cannot('crud', $workout))
        {
            abort(403);
        }

        $workout->load([

            'exercises.results' => function($query) use ($workout){

                $query->where('workout_id', '=', $workout->id);

            },

            'exercises.metrics',

            'user'
        ]);

        return response()->json($workout);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(WorkoutRequest $request, $id)
    {
        $workout = Workout::findOrFail($id);
        $workout->update($request->all());

        $exercises = collect($request->input('exercises'));
        $workout->exercises()->sync($exercises->pluck('id')->toArray());

        $workout->results()->delete();

        foreach ($request->input('exercises') as $exercise)
        {
            foreach ($exercise['metrics'] as $metric)
            {
                $workout->results()->create([
                    'metric_id' => $metric['id'],
                    'exercise_id' => $exercise['id'],
                    'value' => $metric['value']
                ]);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $workout = Workout::findOrFail($id);

        if(Auth::user()->cannot('crud', $workout))
        {
            abort(403);
        }

        $workout->delete();

    }
}
