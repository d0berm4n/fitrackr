<?php

namespace App\Http\Controllers;

use App\Http\Requests\ClientRequest;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class ClientController extends Controller
{
    function __construct()
    {
        $this->middleware('auth');
        $this->middleware('staff');
    }

    /**
     * @return mixed
     */
    public function autocomplete()
    {
        return Auth::user()->clients()->autocomplete()->get()->toArray();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->ajax())
        {
            return Auth::user()->clients->toJson();
        }
        else
        {
            return view('clients.index');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ClientRequest $request)
    {
//        Generate password
        $password = substr(hash('sha512',rand()),0,8);

        $client = User::create([
            'name' => $request->input('username'),
            'email' => $request->input('email'),
            'password' => $password,
        ]);

        Auth::user()->clients()->attach($client);

        Mail::send('mail.newclient', ['staff' => Auth::user(), 'client' => $client, 'password' => $password], function($mail) use ($client) {

            $mail->from('robot@fitrackr.com', 'Fitrackr - best fitness tracker');
            $mail->to($client->email, $client->name)->subject('You are now on fitrackr');

        });
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Auth::user()->clients()->find($id)->toJson();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Auth::user()->clients()->detach($id);
    }
}
