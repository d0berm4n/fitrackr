<?php

namespace App\Http\Controllers;

use App\Http\Requests\TrainingProgramRequest;
use App\TrainingProgram;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class TrainingProgramController extends Controller
{

    function __construct()
    {
        $this->middleware('auth');
        $this->middleware('staff');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->ajax())
        {
            return Auth::user()->programs()->with('category')->get()->toJson();
        }
        else
        {
            return view('trainingprogram.index');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TrainingProgramRequest $request)
    {
        TrainingProgram::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return TrainingProgram::withStrategies($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $program = TrainingProgram::findOrFail($id);
        return view('trainingprogram.edit', compact('program'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $program = TrainingProgram::findOrFail($id);
        $program->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $program = TrainingProgram::findOrFail($id);
        $program->delete();
    }
}
