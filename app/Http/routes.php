<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['api','cors'],'prefix' => 'api'], function () {

    Route::post('register', 'APIController@register');

    Route::post('login', 'APIController@login');

    Route::group(['middleware' => 'jwt-auth'], function () {

        Route::resource('workouts', 'WorkoutController');
        Route::resource('exercises', 'ExerciseController');
        Route::resource('metrics', 'MetricController');
        Route::resource('clients', 'ClientController');
        Route::resource('programs', 'TrainingProgramController');
        Route::resource('categories', 'ProgramCategoryController');
        Route::resource('workouttemplates', 'WorkoutTemplateController');

    });

});

//Route::group(['middleware' => 'web'], function () {
//    Route::auth();
//    Route::get('/', 'HomeController@index');
//    Route::get('/clients/autocomplete', 'ClientController@autocomplete');
//    Route::get('units', function(){
//        return response()->json(Unit::all());
//    });
//
//    Route::resource('workouts', 'WorkoutController');
//    Route::resource('exercises', 'ExerciseController');
//    Route::resource('metrics', 'MetricController');
//    Route::resource('clients', 'ClientController');
//    Route::resource('programs', 'TrainingProgramController');
//    Route::resource('categories', 'ProgramCategoryController');
//    Route::resource('workouttemplates', 'WorkoutTemplateController');
//
//
//});
