<?php

namespace App\Http\ViewComposers;

use App\Category;
use Illuminate\View\View;

class TrainingProgramComposer
{

    public function compose(View $view)
    {
        $view->with('cats', Category::all());
    }

}