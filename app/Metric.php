<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Metric extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['name', 'unit_id'];

    /**
     *Get metric's unit
     */
    public function unit()
    {
        return $this->belongsTo('App\Unit');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function results()
    {
        return $this->hasMany('App\Result');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function strategies()
    {
        return $this->hasMany('App\Strategy');
    }
}
