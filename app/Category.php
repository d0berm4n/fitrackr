<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ["name", "description"];
    /**
     *Get category's exercises
     */
    public function exercises()
    {
        return $this->belongsToMany('App\Exercise');
    }
}
