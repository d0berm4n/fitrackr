<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Result extends Model
{
    public $timestamps = false;

    protected $fillable = ['workout_id', 'exercise_id', 'metric_id', 'value'];

    public function workout()
    {
        return $this->belongsTo('App\Workout');
    }

    public function exercise()
    {
        return $this->belongsTo('App\Exercise');
    }

    public function metric()
    {
        return $this->belongsTo('App\Metric');
    }
}
