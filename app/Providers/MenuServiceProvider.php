<?php

namespace App\Providers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;
use Menu\Menu;

class MenuServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        view()->composer('layouts.nav', function($view)
        {
            $view->with([
                'menu' => $this->navbarLeft(),
                'rightMenu' => $this->navbarRight()
            ]);
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Prepare menu's left part
     */
    protected function navbarLeft()
    {
        $menu = Menu::handler('main');

//        If user is authenticated

        if (Auth::check())
        {
            $menu->add('workouts', trans('nav.dashboard'));
        }

//        If user has role staff

        if (Auth::check() && Auth::user()->isStaff())
        {
            $menu
                ->add('clients', trans('nav.clients'))
                ->add('programs', trans('nav.programs'));
        }

        $menu->addClass('nav navbar-nav');

        return $menu;
    }


    /**
     * Prepare menu's right part
     */
    protected function navbarRight()
    {
        $right = Menu::handler('right');

        $right->addClass('nav navbar-nav navbar-right');

        if (Auth::check())
        {
            $right
                ->add('#', Auth::user()->name . '<span class="caret">', Menu::itemList()
                    ->add('/logout', trans('nav.logout'))
                    ->addClass('dropdown-menu')
                );

            $right
                ->getItemsAtDepth(0)
                ->addClass('dropdown');

            $right
                ->getItemsAtDepth(0)
                ->getContent()
                ->setAttribute('data-toggle', 'dropdown');
        }
        else
        {
            $right
                ->add('/login', trans('nav.login'))
                ->add('/register', trans('nav.register'));
        }

        return $right;
    }
}
