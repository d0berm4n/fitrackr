<?php

namespace App\Policies;

use App\User;
use App\Workout;
use Illuminate\Auth\Access\HandlesAuthorization;

class WorkoutPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param User $user
     * @param Workout $workout
     * @return bool
     */
    public function crud(User $user, Workout $workout)
    {
        return $user->id == $workout->user_id || $user->clients()->find($workout->user_id);
    }
}
