<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrainingProgram extends Model
{
    protected $fillable = ["name", "category_id", "user_id", "description"];

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function workouts()
    {
        return $this->hasMany('App\WorkoutTemplate', 'program_id');
    }

    public static function withStrategies($id)
    {
        $program = self::findOrFail($id);
        $program->load('workouts');

        foreach ($program->workouts as $workout)
        {
            $workout->load('exercises');

            foreach ($workout->exercises as $exercise)
            {
                $exercise->load([
                    'metrics.strategies' => function($query) use ($workout, $exercise){
                        $query->where([
                            ['strategies.workout_template_id','=', $workout->id],
                            ['strategies.exercise_id','=', $exercise->id],
                        ]);
                    },
                    'metrics.unit'
                ]);
            }
        }

        return $program;
    }
}
