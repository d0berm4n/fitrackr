<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\Request;

class Workout extends Model
{
    protected $fillable = ['name', 'description', 'start', 'user_id', 'duration', 'end'];

    /**
     *Get workout's exercises
     */
    public function exercises()
    {
        return $this->morphToMany('App\Exercise', 'exercisable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function results()
    {
        return $this->hasMany('App\Result');
    }

    /**
     * @param $value
     */
    public function setStartAttribute($value)
    {
        $this->attributes['start'] = Carbon::parse($value);
    }

    /**
     * @return mixed
     */
    public static function allWithExercises()
    {
        if(Auth::user()->isStaff())
        {
            $workouts = new Collection();

            Auth::user()->load('clients.workouts')->clients->each(function($client, $key) use (&$workouts)
            {
                $workouts = $workouts->merge(
                    $client
                        ->workouts()
                        ->whereBetween('workouts.start', [request('start'), request('end')])
                        ->get()
                );
            });
        }
        else
        {
            $workouts = Auth::user()
                ->workouts()
//                ->whereBetween('workouts.start', [request('start'), request('end')])
                ->get();
        }

        foreach ($workouts as $workout)
        {
            $workout->load([

                'exercises.results' => function($query) use ($workout){

                    $query->where('workout_id', '=', $workout->id);

                },

                'exercises.metrics.unit',

                'user' => function($query) {

                    $query->select('id', 'name');

                }
            ]);
        }

        return $workouts;
    }
}
