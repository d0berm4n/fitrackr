<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkoutTemplate extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['name', 'description', 'days', 'program_id'];

    /**
     * @param $val
     */
    public function setDaysAttribute($val)
    {
        $this->attributes['days'] = json_encode($val);
    }

    /**
     * @param $val
     * @return mixed
     */
    public function getDaysAttribute($val)
    {
        return json_decode($val);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function strategies()
    {
        return $this->hasMany('App\Strategy');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function exercises()
    {
        return $this->morphToMany('App\Exercise', 'exercisable');
    }

    public function program()
    {
        return $this->belongsTo('App\TrainingProgram');
    }
}
