<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Strategy extends Model
{
    protected $fillable = ['workout_template_id', 'exercise_id', 'metric_id', 'value', 'enabled', 'direction', 'difference', 'units', 'period'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function workoutTempate()
    {
        return $this->belongsTo('App\WorkoutTemplate');
    }

    public function exercise()
    {
        return $this->belongsTo('App\Exercise');
    }
}
