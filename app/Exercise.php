<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exercise extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['name', 'description'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function workouts()
    {
        return $this->morphedByMany('App\Workout', 'exercisable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function workoutTemplates()
    {
        return $this->morphedByMany('App\WorkoutTemplate', 'exercisable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany('App\Category');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function metrics()
    {
        return $this->belongsToMany('App\Metric');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function results()
    {
        return $this->hasMany('App\Result');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function strategies()
    {
        return $this->hasMany('App\Strategy');
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeAutocomplete($query)
    {
        return $query->where('name', 'like', '%' . request()->input('query') . '%');
    }
}
