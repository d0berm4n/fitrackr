<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = ['name'];
    /**
     *Get users which have this role
     */
    public function users()
    {
        return $this->belongsToMany('App\User');
    }
}
