<?php
/**
 * Created by PhpStorm.
 * User: Max
 * Date: 04.01.2016
 * Time: 17:45
 */

return [

    'home' => 'Главная',
    'login' => 'Войти',
    'register' => 'Зарегистрироваться',
    'logout' => 'Выйти',
    'dashboard' => 'Расписание',
    'clients' => 'Клиенты',
    'programs' => 'Программы',

];