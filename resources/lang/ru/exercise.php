<?php
/**
 * Created by PhpStorm.
 * User: maxim.ivassenko
 * Date: 15.01.16
 * Time: 11:57
 */

return [

    'dialogHeader' => 'Упражнение',
    'metric_placeholder' => 'Начните вводить название метрики',
    'metric' => 'Метрика'

];