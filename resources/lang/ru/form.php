<?php
/**
 * Created by PhpStorm.
 * User: Max
 * Date: 07.01.2016
 * Time: 12:56
 */

return [

    'name' => 'Название:',
    'name_ph' => 'Например "Тренировка ног"',

    'username' => 'Имя:',
    'username_ph' => 'Вася Пупкин',

    'email' => 'Почта',
    'email_ph' => 'john@doe.example',

    'description' => 'Описание:',
    'description_ph' => 'Например "Развиваем силу и выносливость"',

    'category' => 'Категория:',
    'daysOfWeek' => 'Дни недели:',

    'start_at' => 'Начало:',
    'duration' => 'Продолжительность:',
    'exercises' => 'Упражнения:',
    'metrics' => 'Метрики:',
    'unit' => 'Еденица измерения:',
    'client' => 'Клиент:',

    "save" => "Сохранить",
    "cancel" => "Отмена",
    "close" => "Закрыть",
    "delete" => "Удалить",

];