<div id="templateExerciseAlertTemplate" class="alert alert-info alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <a href="#templateMetrics" class="exerciseName" role="button" data-toggle="collapse" aria-expanded="false"></a>
    <div id="templateMetrics" class="tmpmetrics collapse"></div>
</div>

<div id="templateExerciseMetricTemplate" class="metric">

    <div class="form-group">
        <label class="control-label"></label>
        <input type="text" class="form-control">
    </div>

    <label>
        <input class="strategyCheck" type="checkbox">&nbsp;<i class="fa fa-line-chart"></i>&nbsp;Стратегия
    </label>

    <div class="strategyWrapper form-inline" style="display: none;">

        <div class="form-group">
            <select class="direction form-control input-sm">
                <option value="up">увеличение</option>
                <option value="down">уменьшение</option>
            </select>
        </div>

        <div class="form-group">
            <label class="control-label">на</label>
            <input type="text" class="difference form-control input-sm">
        </div>

        <div class="form-group">
            <select class="units form-control input-sm">
                <option value="unit" selected>кг</option>
                <option value="percent">%</option>
            </select>
        </div>

        <div class="form-group">
            <select class="period form-control input-sm">
                <option value="week" selected>в неделю</option>
                <option value="month">в месяц</option>
            </select>
        </div>

    </div>

</div>


