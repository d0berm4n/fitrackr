<div id="workoutTemplateDialog" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">{{ trans('workout.dialogHeader') }}</h4>
            </div>

            <div class="modal-body">

                @include('partials.errors')

                <form id="workoutTemplateForm">

                    <input type="hidden" name="id">

                    <div class="form-group">
                        <label for="name">{{ trans('form.name') }}</label>
                        <input type="text" name="name" class="form-control" placeholder="{{ trans('form.name_ph') }}">
                    </div>

                    <div class="form-group">
                        <label for="description">{{ trans('form.description') }}</label>
                        <input type="text" name="description" class="form-control" placeholder="{{ trans('form.description_ph') }}">
                    </div>

                    <div class="form-group">
                        <label for="workoutDays">{{ trans('form.daysOfWeek') }}</label>
                        <div id="workoutDays" class="checkbox">
                            <label>
                                <input data-day="1" type="checkbox"> Пн
                            </label>

                            <label>
                                <input data-day="2" type="checkbox"> Вт
                            </label>

                            <label>
                                <input data-day="3" type="checkbox"> Ср
                            </label>

                            <label>
                                <input data-day="4" type="checkbox"> Чт
                            </label>

                            <label>
                                <input data-day="5" type="checkbox"> Пт
                            </label>

                            <label>
                                <input data-day="6" type="checkbox"> Сб
                            </label>

                            <label>
                                <input data-day="7" type="checkbox"> Вс
                            </label>
                        </div>
                    </div>

                    <div class="form-group">

                        <label for="templateExercises">{{ trans('form.exercises') }}</label>

                        <div id="templateExercises"></div>

                        <div id="addExerciseToTemplateBtn" class="btn btn-primary"><i class="glyphicon glyphicon-plus"></i></div>

                    </div>

                </form>

                {{--Partial templates--}}

                <div class="hidden">

                    @include('workouttemplate.exercise')

                    <div id="exercisePopover" class="input-group">

                        <input id="exerciseInput" class="form-control" placeholder="{{ trans('workout.exercise_placeholder') }}" type="text">

                        <span class="input-group-btn">
                            <div id="createNewWorkoutExercise" data-dismiss="modal" data-toggle="modal" data-target="#exerciseDialog" class="btn btn-primary"><i class="glyphicon glyphicon-plus"></i></div>
                        </span>

                    </div>

                </div>

                {{----}}

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('form.cancel') }}</button>
                <button id="workoutTwmplateDialogOKBtn" type="button" class="btn btn-primary">{{ trans('form.save') }}</button>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->