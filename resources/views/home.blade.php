@extends('layouts.app')


@section('content')

    <div class="jumbotron">
        <div class="row">
            <div class="col-md-12">
                <h1 class="text-center">Fitrackr - облегчает жизнь</h1>
            </div>
            <div class="col-md-8 col-md-offset-2">
                <p>Теперь нет необходимости вести кучу записей в тетрадях. Создавайте тренировочные программы, составляйте расписание занятий и контролируйте прогресс ваших клиентов в удобном виде всего в несколько кликов!</p>
            </div>

        </div>
    </div>

@endsection
