<div id="workoutDialog" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">{{ trans('workout.dialogHeader') }}</h4>
            </div>

            <div class="modal-body">

                @include('partials.errors')

                <form id="workoutForm">

                    <input type="hidden" name="id">

                    <div class="form-group">
                        <input id="user_id" type="hidden" name="user_id">
                        @if(Auth::user()->isStaff())
                            <label for="usernameInput">{{ trans('form.client') }}</label>
                            <input id="usernameInput" class="form-control" placeholder="{{ trans('workout.username_placeholder') }}" type="text">
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="name">{{ trans('form.name') }}</label>
                        <input type="text" name="name" class="form-control" placeholder="{{ trans('form.name_ph') }}">
                    </div>

                    <div class="form-group">
                        <label for="description">{{ trans('form.description') }}</label>
                        <input type="text" name="description" class="form-control" placeholder="{{ trans('form.description_ph') }}">
                    </div>

                    <div class="form-group">
                        <label for="start">{{ trans('form.start_at') }}</label>
                        <div class="input-group">
                            <input id="start" type="datetime" name="start" class="form-control">
                            <div class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="duration">{{ trans('form.duration') }}</label>
                        <div class="input-group">
                            <input id="duration" type="text" name="duration" class="form-control">
                            <div class="input-group-addon"><i class="glyphicon glyphicon-time"></i></div>
                        </div>
                    </div>



                    <div class="form-group">

                        <label for="exercises">{{ trans('form.exercises') }}</label>

                        <div id="exercises"></div>

                        <div id="addExerciseBtn" class="btn btn-primary"><i class="glyphicon glyphicon-plus"></i></div>

                    </div>

                </form>

                {{--Partial templates--}}

                <div class="hidden">

                    @include('workout.exercise')

                    <div id="exercisePopover" class="input-group">

                        <input id="exerciseInput" class="form-control" placeholder="{{ trans('workout.exercise_placeholder') }}" type="text">

                        <span class="input-group-btn">
                            <div id="createNewWorkoutExercise" data-dismiss="modal" data-toggle="modal" data-target="#exerciseDialog" class="btn btn-primary"><i class="glyphicon glyphicon-plus"></i></div>
                        </span>

                    </div>

                </div>

                {{----}}

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('form.cancel') }}</button>
                <button id="workoutDialogOKBtn" type="button" class="btn btn-primary">{{ trans('form.save') }}</button>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->