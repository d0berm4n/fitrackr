@extends('layouts.c10o1')
@include('workout.dialog')
@include('workout.client')
@include('exercise.dialog')
@include('exercise.metricDialog')
@section('body')

        <button onclick="addWorkout()" class="btn btn-primary">
            <i class="fa fa-plus"></i>&nbsp;{{ trans('workout.add') }}
        </button >

        <div id="calendar"></div>


@endsection