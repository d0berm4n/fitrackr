<div id="workoutClientDialog" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">{{ trans('workout.dialogHeader') }}</h4>
            </div>

            <div class="modal-body">
                <input type="hidden" id="workoutId">
                <div id="exercisesClient"></div>
            </div>

            <div class="modal-footer">
                <div class="btn-group pull-left">
                    <button id="editWorkout" class="btn btn-info"><i class="glyphicon glyphicon-cog"></i></button>
                    <button id="deleteWorkout" class="btn btn-danger"><i class="glyphicon glyphicon-trash"></i></button>
                </div>
                <button data-dismiss="modal" type="button" class="btn btn-primary">{{ trans('form.close') }}</button>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->