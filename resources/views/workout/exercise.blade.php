<div id="exerciseAlertTemplate" class="alert alert-info alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <a class="exerciseName" role="button" data-toggle="collapse" aria-expanded="false"></a>
    <div class="exerciseMetrics collapse"></div>
</div>

<div id="exerciseAlertClientTemplate" class="alert alert-info alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
</div>

<div id="exerciseMetricTemplate" class="metric">
    <label></label>
    <input type="text" class="form-control">
</div>