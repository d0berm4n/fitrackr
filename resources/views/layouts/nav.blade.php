<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#spark-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        <i class="fa fa-heartbeat text-primary"></i>&nbsp;Fitrackr
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="spark-navbar-collapse">

                    {!! $menu !!}
                    {!! $rightMenu !!}

                </div>
            </div>
        </div>

    </div>
</nav>

