<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Fitrackr</title>

    <!-- Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>

    <!-- Styles -->
    <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">


    {{--Scripts--}}
    <script src="{{ asset('js/jquery-2.2.3.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>

    {{--Calendar--}}
    <script src="{{ asset('js/fullcalendar-2.5.0/lib/moment.min.js') }}"></script>
    <script src="{{ asset('js/fullcalendar-2.5.0/fullcalendar.js') }}"></script>
    <script src="{{ asset('js/fullcalendar-2.5.0/lang-all.js') }}"></script>
    <link href="{{ asset('js/fullcalendar-2.5.0/fullcalendar.css') }}" rel="stylesheet">
    <link href="{{ asset('js/fullcalendar-2.5.0/fullcalendar.print.css') }}" rel="stylesheet" media="print">

    {{--Datetimepicker--}}
    <script src="{{ asset('js/datetimepicker/bootstrap-datetimepicker.min.js') }}"></script>
    <link href="{{ asset('js/datetimepicker/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">

    {{--typeahead--}}
    <script src="{{ asset('js/typeahead.js/typeahead.jquery.js') }}"></script>
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">

</head>
<body id="app-layout">

    @include('layouts.nav')



    <div class="container-fluid">
        @yield('content')
    </div>

    <div id="fullpageloader"></div>

    <!-- JavaScripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/errors.js') }}"></script>
    <script src="{{ asset('js/workouts.js') }}"></script>
    <script src="{{ asset('js/exercises.js') }}"></script>
    <script src="{{ asset('js/workoutTemplates.js') }}"></script>
    <script src="{{ asset('js/clients.js') }}"></script>
    <script src="{{ asset('js/trainingPrograms.js') }}"></script>

</body>
</html>
