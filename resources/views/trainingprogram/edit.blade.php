@extends('layouts.c10o1')

@section('body')

    @include('workouttemplate.dialog')

    <div class="back_button">
        <a class="btn btn-default btn-sm" href="/programs"><i class="glyphicon glyphicon-chevron-left"></i>Назад</a>
    </div>

    <input type="hidden" name="pID" value="{{ $program->id }}">

    {{--Category--}}
    <div class="editable_view">
        <span class="label label-info">{{ $program->category->name }}</span>
        &nbsp;<span class="glyphicon glyphicon-pencil editable_edit_btn"></span>
    </div>

    <div class="editable_controls form-group row">
        <span class="col-md-10">
            <select name="category_id" class="form-control">
                @foreach($cats as $category)
                    <option value="{{ $category->id }}" {{ $program->category->id == $category->id ? 'selected' : '' }}>{{ $category->name }}</option>
                @endforeach
            </select>
        </span>
        <span class="col-md-2">
            <div class="btn btn-primary editable_save_btn">Сохранить</div>
        </span>
    </div>

    {{--Name--}}
    <div class="editable_view">
        <h1>{{ $program->name }}&nbsp;<small><span class="glyphicon glyphicon-pencil editable_edit_btn"></span></small></h1>

    </div>

    <div class="editable_controls form-group row">
        <span class="col-md-10">
            <input class="form-control" type="text" name="p_name" value="{{ $program->name }}">
        </span>
        <span class="col-md-2">
            <div class="btn btn-primary editable_save_btn">Сохранить</div>
        </span>
    </div>

    {{--Description--}}
    <div class="editable_view">
        <p class="lead">{{ $program->description }}&nbsp;<small><span class="glyphicon glyphicon-pencil editable_edit_btn"></span></small></p>

    </div>

    <div class="editable_controls form-group row">
        <span class="col-md-10">
            <input class="form-control" type="text" name="p_description" value="{{ $program->description }}">
        </span>
        <span class="col-md-2">
            <div class="btn btn-primary editable_save_btn">Сохранить</div>
        </span>
    </div>



    {{--Workouts--}}
    <h3>Занятия:</h3>

    <div>
        <button class="btn btn-primary" onclick="addWorkoutToProgram()">
            <i class="fa fa-plus"></i>&nbsp;Добавить тренировку
        </button>
    </div>

    <div id="programWorkouts"></div>

    <div class="hidden">

        <div class="workoutTemplateExercise">
            <div class="">
                <a class="exerciseHeading" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample"></a>
                <div class="collapse" id="collapseExample"></div>
            </div>
        </div>

        <div class="workoutTemplate" style="display:none">
            <div class="panel panel-default">

                <div class="panel-body"></div>

                <div class="panel-footer">
                    <i class="fa fa-calendar"></i>
                </div>

            </div>
        </div>

    </div>

@endsection
