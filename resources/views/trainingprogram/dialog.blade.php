<div id="programDialog" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Тренировочная программа</h4>
            </div>

            <div class="modal-body">

                @include('partials.errors')

                <form id="programForm">

                    <input type="hidden" name="id">

                    <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">

                    <div class="form-group">
                        <label for="name">{{ trans('form.name') }}</label>
                        <input type="text" name="name" class="form-control" placeholder="{{ trans('form.name_ph') }}">
                    </div>

                    <div class="form-group">
                        <label for="description">{{ trans('form.description') }}</label>
                        <input type="text" name="description" class="form-control" placeholder="{{ trans('form.description_ph') }}">
                    </div>

                    <div class="form-group">
                        <label for="category_id">{{ trans('form.category') }}</label>
                        <div class="input-group">
                            <select name="category_id" class="form-control"></select>
                            <span class="input-group-btn">
                                <button class="btn btn-primary" type="button"><i class="glyphicon glyphicon-plus"></i>&nbsp;</button>
                            </span>
                        </div>

                    </div>


                </form>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('form.cancel') }}</button>
                <button id="programDialogOKBtn" type="button" class="btn btn-primary">{{ trans('form.save') }}</button>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->