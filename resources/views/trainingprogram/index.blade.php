@extends('layouts.c10o1')
@include('trainingprogram.dialog')

@section('body')

    <button class="btn btn-primary" onclick="addTrainingProgram()">
        <i class="fa fa-plus"></i>&nbsp;Добавить программу
    </button>

    <table id="programsTable" class="table table-hover">

        <thead>
            <tr>
                <th>Название</th>
                <th>Категория</th>
                <th>Описание</th>
                <th></th>
            </tr>
        </thead>

        <tbody></tbody>

    </table>

@endsection