<div id="clientDialog" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Клиент</h4>
            </div>

            <div class="modal-body">

                @include('partials.errors')

                <form id="clientForm">

                    <input type="hidden" name="id">

                    <div class="form-group">
                        <label for="name">{{ trans('form.username') }}</label>
                        <input type="text" name="username" class="form-control" placeholder="{{ trans('form.username_ph') }}">
                    </div>

                    <div class="form-group">
                        <label for="name">{{ trans('form.email') }}</label>
                        <input type="email" name="email" class="form-control" placeholder="{{ trans('form.email_ph') }}">
                        <p class="help-block">На эту почту будет выслано письмо с паролем</p>
                    </div>

                </form>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('form.cancel') }}</button>
                <button id="clientDialogOKBtn" type="button" class="btn btn-primary">{{ trans('form.save') }}</button>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->