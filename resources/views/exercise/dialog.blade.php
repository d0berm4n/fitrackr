<div id="exerciseDialog" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">{{ trans('exercise.dialogHeader') }}</h4>
            </div>

            <div class="modal-body">

                @include('partials.errors')

                <form id="exerciseForm">

                    <div class="form-group">
                        <input type="text" name="name" class="form-control" placeholder="{{ trans('form.name') }}">
                    </div>

                    <div class="form-group">
                        <input type="text" name="description" class="form-control" placeholder="{{ trans('form.description') }}">
                    </div>

                    <div class="form-group">

                        <label for="metrics">{{ trans('form.metrics') }}</label>

                        <div id="metrics"></div>

                        <div id="addMetricsBtn" class="btn btn-primary"><i class="glyphicon glyphicon-plus"></i></div>

                    </div>

                </form>

                {{--Partial templates--}}

                <div class="hidden">

                    <div id="metricPopover" class="input-group">

                        <input id="metricInput" class="form-control" placeholder="{{ trans('exercise.metric_placeholder') }}" type="text">

                        <span class="input-group-btn">
                            <div id="createNewExerciseMetric" data-dismiss="modal" data-toggle="modal" data-target="#metricDialog" class="btn btn-primary"><i class="glyphicon glyphicon-plus"></i></div>
                        </span>

                    </div>

                    <div id="metricAlertTemplate" class="alert alert-info alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>

                </div>

                {{----}}

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" data-toggle="modal" data-target="#workoutDialog">{{ trans('form.cancel') }}</button>
                <button id="exerciseDialogOKBtn" type="button" class="btn btn-primary">{{ trans('form.save') }}</button>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->