<div id="metricDialog" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">{{ trans('exercise.metric') }}</h4>
            </div>

            <div class="modal-body">

                @include('partials.errors')

                <form id="metricForm">

                    <div class="form-group">
                        <input type="text" name="name" class="form-control" placeholder="{{ trans('form.name') }}">
                    </div>

                    <div class="form-group">
                        <label for="unit_id">{{ trans('form.unit') }}</label>
                        <select name="unit_id" class="form-control"></select>
                    </div>

                </form>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-toggle="modal" data-target="#exerciseDialog" data-dismiss="modal">{{ trans('form.cancel') }}</button>
                <button id="metricDialogOKBtn" type="button" class="btn btn-primary">{{ trans('form.save') }}</button>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->