/**
 * Created by maxim.ivassenko on 15.01.16.
 */

$(document).ready(function() {

    // Render calendar
    $('#calendar').fullCalendar({
        firstDay: 1,
        lang: 'ru',
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },
        defaultDate: moment(),
        defaultView: 'month',
        editable: true,
        events: loadWorkouts,
        timeFormat: 'H:mm',
        eventClick: function(event)
        {
            viewWorkout(event);
        },
        eventRender: function(event, element)
        {
            $(element).popover({
                title: event.user.name,
                content: function()
                {
                    var html = event.description + '<br>';

                    $.each(event.exercises, function ()
                    {
                        html += this.name + '<br>';
                    });

                    return html;
                },
                placement: 'top',
                html: true,
                trigger: 'hover'
            });
        },
        eventAfterRender: function(event, element, view)
        {
            console.log(view);
        }
    });

    // Datetimepickers
    $('input[type=datetime]').datetimepicker({
        locale: 'ru',
        defaultDate: moment(),
        minDate: moment()
    });

    $('#duration').datetimepicker({
        locale: 'ru',
        format: 'H:m:s'
    });

    // Save workout
    $('#workoutDialogOKBtn').on({
        click: function()
        {
            var exercises = [],
                // Check if id is defined and generate url part for request
                id = $('#workoutForm input[name=id]').val() == '' ? '' : '/' + $('#workoutForm input[name=id]').val(),
                // If update than should be PUT method used
                method = $('#workoutForm input[name=id]').val() == '' ? 'POST' : 'PUT';

            $.each($('#exercises').children(), function(){
                var exercise = {};

                exercise.id = $(this).data('id');
                exercise.metrics = [];

                $.each($(this).find('input'), function(){
                    exercise.metrics.push({
                        id: $(this).data('id'),
                        value: $(this).val()
                    });
                });

                exercises.push(exercise);
            });

            $.ajax({
                url: 'workouts' + id +'?' + $('#workoutForm').serialize(),
                type: method,
                data:
                {
                    exercises: exercises
                },
                success: function()
                {
                    $('#workoutDialog').modal('hide');
                    $('#calendar').fullCalendar('refetchEvents');
                },
                error: function(jqXHR)
                {
                    showErrors(jqXHR.responseJSON);
                }
            });
        }
    });

    // Edit workout
    $('#editWorkout').on('click', function () {
        $.ajax({
            url: 'workouts/' + $('#workoutId').val(),
            type: 'GET',
            dataType: 'json',
            success: function (workout)
            {
                $('#workoutClientDialog').modal('hide');
                clearWorkoutDialog();
                $('#workoutDialog').modal('show');

                $('#workoutForm input[name=id]').val(workout.id);
                $('#usernameInput').val(workout.user.name);
                $('#user_id').val(workout.user.id);
                $('#workoutForm input[name=name]').val(workout.name);
                $('#workoutForm input[name=description]').val(workout.description);
                $('#start').val(workout.start);
                $('#duration').val(workout.duration);

                $.each(workout.exercises, function(){
                    var exercise = this;
                    addExerciseToWorkout(exercise);

                    $.each(exercise.results, function () {
                        $('#exercises .alert[data-id=' + this.exercise_id + '] .metric input[data-id=' + this.metric_id + ']').val(this.value);
                    });
                });


            }
        });
    });

    // Delete workout
    $('#deleteWorkout').on('click', function () {
        if(confirm('Удалить тренировку?'))
        {
            $.ajax({
                url: 'workouts/' + $('#workoutId').val(),
                type: 'DELETE',
                success: function()
                {
                    $('#workoutClientDialog').modal('hide');
                    $('#calendar').fullCalendar('refetchEvents');
                }
            });
        }
    });

    // Exercises popover
    $('#addExerciseBtn').popover({
        html: true,
        content: $('#exercisePopover')
    });

    $('#addExerciseBtn').on('shown.bs.popover', function() {

        autocomplete({
            element: '#exerciseInput',
            url: 'exercises',
            callback: function(item)
            {
                // If use .popover('hide') method popover appear only after 2nd click then
                $('#addExerciseBtn').trigger('click');
                addExerciseToWorkout(item);
            }
        });

        $('#createNewWorkoutExercise').off('click');
        $('#createNewWorkoutExercise').on('click', function () {
            // If use .popover('hide') method popover appear only after 2nd click then
            $('#addExerciseBtn').trigger('click');
            clearExerciseDialog();
        });
    });

    // Client name autocomplete
    autocomplete({
        element: '#usernameInput',
        url: 'clients/autocomplete',
        callback: function(item)
        {
            $('#user_id').val(item.id);
        },
        noclear: true
    });

});

function loadWorkouts(start, end, timezone, callback)
{
    $.ajax({
        url: 'workouts',
        type: 'GET',
        dataType: 'json',
        data: {
            start: start.format('YYYY.MM.DD'),
            end: end.format('YYYY.MM.DD')
        },
        success: function(workouts) {
            $.each(workouts, function(){
                this.title = this.name;
            });

            callback(workouts);
        }
    });
}

function clearWorkoutDialog()
{
    //Clear input fields
    $('#workoutForm').find('input').val(null);

    //Clear errors
    clearErrors();

    //Clear exercises
    $('#exercises').html(null);

    //Clear client inputs
    //$('#user_id, #usernameInput').val(null);
}

function addWorkout()
{
    clearWorkoutDialog();
    $('#workoutDialog').modal('show');
}

function viewWorkout(workout)
{
    var wrapper = $('#exercisesClient');
    wrapper.html(null);
    $('#workoutId').val(workout.id);
    $('#workoutClientDialog .modal-title').text(workout.name);

    if(workout.description != '')
    {
        $('<p>').addClass('leed').text(workout.description).appendTo(wrapper);
    }

    $.each(workout.exercises, function(){

        var exerciseItem = $('<div class="well">');
        $('<p class="exerciseHeader">').text(this.name).appendTo(exerciseItem);

        //$('<hr>').appendTo(wrapper);
        //$('<h3>').text(this.name).appendTo(wrapper);


        exercise = this;

        $.each(this.metrics, function(){

            var value = exercise.results[exercise.metrics.indexOf(this)].value;

            $('<h4>').text(this.name + ' : ').append($('<span class="label label-info">').text(value + ' ' + this.unit.name)).appendTo(exerciseItem);

        });

        exerciseItem.appendTo(wrapper);

    });

    $('#workoutClientDialog').modal('show');
}

function addExerciseToWorkout(exercise)
{
    var template = $('#exerciseAlertTemplate').clone(),
        exerciseNodeId = 'ex_' + exercise.id;

    template.removeAttr('id');
    template.attr('data-id', exercise.id);
    template.children('.exerciseName').text(exercise.name);

    if(exercise.metrics.length > 0)
    {
        $.each(exercise.metrics, function() {
            var metricTemplate = $('#exerciseMetricTemplate').clone();
            metricTemplate.removeAttr('id');
            metricTemplate.children('label').text(this.name);
            metricTemplate.children('input').attr('data-id', this.id);
            metricTemplate.appendTo(template.children('.exerciseMetrics'));
        });

        template.children('.exerciseMetrics').attr('id', exerciseNodeId);
        template.children('[data-toggle=collapse]').attr('href', '#' + exerciseNodeId);
        template.children('[data-toggle=collapse]').attr('aria-controls', exerciseNodeId);
    }

    template.appendTo($('#exercises'));
}