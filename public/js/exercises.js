/**
 * Created by maxim.ivassenko on 15.01.16.
 */

$(document).ready(function () {


    $('#addMetricsBtn').popover({
        html: true,
        content: $('#metricPopover')
    });

    $('#addMetricsBtn').on('shown.bs.popover', function () {
        autocomplete({
            element: '#metricInput',
            url: 'metrics',
            callback: function(item)
            {
                //If use .popover('hide') method popover appear only after 2nd click then
                $('#addMetricsBtn').trigger('click');
                addMetricToExercise(item);
            }
        });
    });
    
    $('#exerciseDialogOKBtn').on('click', function () {
        var metrics = [];

        $.each($('#metrics').children(), function(){
            metrics.push($(this).data('id'));
        });

        $.ajax({
            url: 'exercises?' + $('#exerciseForm').serialize(),
            type: 'POST',
            dataType: 'json',
            data: {
                metrics: metrics
            },
            success: function(exercise)
            {
                addExerciseToWorkout(exercise);
                $('#exerciseDialog').modal('hide');
                $('#workoutDialog').modal('show');
            },
            error: function(jqXHR)
            {
                showErrors(jqXHR.responseJSON);
            }
        });
    });

    $('#createNewExerciseMetric').on('click', function() {
        $('#addMetricsBtn').trigger('click');
    });

    $('#metricDialog').on('show.bs.modal', function () {
        clearErrors();
        $('#metricForm').find('input[type=text], select').val(null);

        $.ajax({
            url: 'units',
            type: 'GET',
            dataType: 'json',
            success: function(units)
            {
                $('#metricForm select').html(null);
                $.each(units, function(){
                    $('<option>').val(this.id).text(this.name).appendTo($('#metricForm select'));
                });
            }
        });
    });

    $('#metricDialogOKBtn').on('click', function(){
        $.ajax({
            url: 'metrics?' + $('#metricForm').serialize(),
            type: 'POST',
            dataType: 'json',
            success: function(metric)
            {
                $('#metricDialog').modal('hide');
                $('#exerciseDialog').modal('show');
                addMetricToExercise(metric);
            }
        });
    });

});

function clearExerciseDialog()
{
    $('#metrics').html(null);
    clearErrors();
    $('#exerciseForm').find('input[type=text]').val(null);
}

function addMetricToExercise(metric)
{
    var template = $('#metricAlertTemplate').clone();
    template.removeAttr('id');
    template.attr('data-id', metric.id);
    template.append(metric.name + ' (' + metric.unit.name + ')');
    $('#metrics').append(template);
}