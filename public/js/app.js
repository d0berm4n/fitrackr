$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },

    cache: false
});

$( document ).ajaxStart(function() {
    //console.log('ajaxStart');
    $( "#fullpageloader" ).show();
});

$( document ).ajaxComplete(function() {
    //console.log('ajaxComplete');
    $( "#fullpageloader" ).hide();
});

moment.locale('ru');

// Fix modal scrolling issue

$(document)
    .on('shown.bs.modal', '.modal', function () {
        $(document.body).addClass('modal-open')
    })
    .on('hidden.bs.modal', '.modal', function () {
        $(document.body).removeClass('modal-open')
    });

function autocomplete(obj)
{
    $(obj.element).typeahead('destroy').off('typeahead:select');
    $(obj.element).typeahead(
        {
            highlight: true
        },
        {
            name: 'ajaxSet',

            async: true,

            source: function(query, syncResults, asyncResults)
            {
                $.ajax({
                    url: obj.url,
                    type: 'GET',
                    dataType: 'json',
                    data: {
                        query: query
                    },
                    success: function(data)
                    {
                        asyncResults(data);
                    }
                });
            },

            display: function(item)
            {
                return item.name;
            },

            limit: 5

        })
        .on('typeahead:select', function(event, item){
            obj.callback(item);
            if(!obj.noclear)
            {
                $(obj.element).typeahead('val', null);
            }
        });
}

