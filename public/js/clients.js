/**
 * Created by maxim.ivassenko on 05.02.16.
 */

$(document).ready(function () {

    if(location.pathname == '/clients')
    {
        loadClients();
    }


    $('#clientDialogOKBtn').on('click', function () {
        var id = $('#clientForm input[name=id]').val() == '' ? '' : '/' + $('#clientForm input[name=id]').val(),
            method = $('#clientForm input[name=id]').val() == '' ? 'POST' : 'PUT';

        $.ajax({
            url: 'clients' + id + '?' + $('#clientForm').serialize(),
            type: method,
            success: function()
            {
                $('#clientDialog').modal('hide');
                loadClients();
            },
            error: function(jqXHR)
            {
                showErrors(jqXHR.responseJSON);
            }
        });
    });

});

function addClient()
{
    clearErrors();
    $('#clientForm input').val(null);
    $('#clientDialog').modal('show');
}

function loadClients()
{
    $.ajax({
        url: 'clients',
        type: 'GET',
        dataType: 'json',
        success: function (clients)
        {
            $('#clientsTable tbody').html(null);

            if (clients.length > 0)
            {
                $.each(clients, function(){
                    $('#clientsTable tbody').append(
                        $('<tr>').append(
                            $('<td>').text(this.name),
                            $('<td>').text(this.email),
                            $('<td>').append(
                                $('<a onclick="editClient(' + this.id + ')" class="clientIcons"><i class="glyphicon glyphicon-pencil"></a>'),
                                $('<a onclick="deleteClient(' + this.id + ')" class="clientIcons"><i class="glyphicon glyphicon-trash"></a>')
                            )
                        )
                    );
                });
            }
            else
            {
                $('#clientsTable tbody').append(
                    $('<tr>').append(
                        $('<td colspan="2">').text('У вас пока нет клиентов')
                    )
                );
            }
        }
    });
}

function editClient(id)
{
    $.ajax({
        url: 'clients/' + id,
        type: 'GET',
        dataType: 'json',
        success: function (client)
        {
            clearErrors();
            $('#clientForm input[name=id]').val(client.id);
            $('#clientForm input[name=username]').val(client.name);
            $('#clientForm input[name=email]').val(client.email);
            $('#clientDialog').modal('show');
        }
    });
}

function deleteClient(id)
{
    if (confirm('Вы уверены?'))
    {
        $.ajax({
            url: 'clients/' + id,
            type: 'DELETE',
            success: function () {
                loadClients();
            }
        });
    }
}


