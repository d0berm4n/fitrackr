/**
 * Created by maxim.ivassenko on 15.01.16.
 */

function clearErrors()
{
    $('.errors ul').html(null);
    $('.errors').hide();
}

function showErrors(errors)
{
    clearErrors();
    var errorsList = $('.errors ul');

    $.each(errors, function(){
        errorsList.append('<li>' + this[0] + '</li>');
    });

    $('.errors').show();
}