/**
 * Created by maxim.ivassenko on 27.01.16.
 */

$(document).ready(function(){

    $('#addExerciseToTemplateBtn').popover({
        html: true,
        content: $('#exercisePopover')
    });

    $('#addExerciseToTemplateBtn').on('shown.bs.popover', function() {
        autocomplete({
            element: '#exerciseInput',
            url: '/exercises',
            callback: function(item)
            {
                //If use .popover('hide') method popover appear only after 2nd click then
                $('#addExerciseToTemplateBtn').trigger('click');
                addExerciseToWorkoutTemplate(item);
            }
        });
    });

    $('#workoutTwmplateDialogOKBtn').on('click', saveWorkoutTemplate);

    $('#workoutTemplateDialog').on('show.bs.modal', clearErrors);

});

function addWorkoutTemplate()
{
    $('#workoutTemplateDialog').modal('show');
}

function addExerciseToWorkoutTemplate(exercise)
{
    var template = $('#templateExerciseAlertTemplate').clone(),
        exerciseNodeId = 'tmpex_' + exercise.id;

    template.removeAttr('id');
    template.attr('data-id', exercise.id);
    template.children('.exerciseName').text(exercise.name);

    if(exercise.metrics.length > 0)
    {
        $.each(exercise.metrics, function() {
            var metricTemplate = $('#templateExerciseMetricTemplate').clone();
            metricTemplate.removeAttr('id');
            metricTemplate.find('label').first().text(this.name);
            metricTemplate.find('input').first().attr('data-id', this.id);
            metricTemplate.appendTo(template.children('.tmpmetrics'));
            metricTemplate.find('.units option[value=unit]').text(this.unit.name);
        });

        template.children('.tmpmetrics').attr('id', exerciseNodeId);
        template.children('[data-toggle=collapse]').attr('href', '#' + exerciseNodeId);
        template.children('[data-toggle=collapse]').attr('aria-controls', exerciseNodeId);
    }

    template.appendTo($('#templateExercises'));

    $('.strategyCheck').on('click', function(){
        var strategy = $(this).parents('.metric').find('.strategyWrapper');
        $(this).prop('checked') ? strategy.show() : strategy.hide();
    });
}

function saveWorkoutTemplate()
{
    var days = [],
        exercises = [];

    $('#workoutDays input:checked').each(function(){
        days.push($(this).data('day'));
    });

    $('#templateExercises').children().each(function(){

        var exercise = {
            id: $(this).data('id'),
            metrics: []
        };

        $(this).find('.metric').each(function () {
            exercise.metrics.push({
                id: $(this).find('input').first().data('id'),
                value: $(this).find('input').first().val(),
                enabled: $(this).find('.strategyCheck').prop('checked'),
                direction: $(this).find('.direction').val(),
                difference: $(this).find('.difference').val(),
                units: $(this).find('.units').val(),
                period: $(this).find('.period').val()
            });
        });

        exercises.push(exercise);
    });

    $.ajax({
        url: '/workouttemplates?' + $('#workoutTemplateForm').serialize(),
        type: 'POST',
        data: {
            program_id: $('input[name=pID]').val(),
            days: days,
            exercises: exercises
        },
        success: function() {
            $('#workoutTemplateDialog').modal('hide');
            clearWorkoutTemplateDialog();
        }
    });
}

function editWorkoutTemplate(id)
{
    $.ajax({
        url: '/workouttemplates/' + id + '/edit'
    });
}

function deleteWorkoutTemplate(id)
{
    if (confirm('Удалить тренировку?'))
    {
        $.ajax({
            url: '/workouttemplates/' + id,
            type: 'delete',
            success: function()
            {
                loadProgramWorkouts();
            }
        });
    }
}

function clearWorkoutTemplateDialog()
{
    $('#templateExercises').html(null);
    $('#workoutTemplateForm input').val(null);
    $('#workoutTemplateForm input[type=checkbox]').prop('checked', false);
}

