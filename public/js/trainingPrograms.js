/**
 * Created by maxim.ivassenko on 12.02.16.
 */

$(document).ready(function () {

    if(location.pathname == '/programs')
    {
        loadPrograms();
    }

    if (location.pathname.indexOf('programs') > 0 && location.pathname.indexOf('edit') > 0)
    {
        loadProgramWorkouts();
    }

    $('#programDialogOKBtn').on('click', function(){
        $.ajax({
            url: 'programs?' + $('#programForm').serialize(),
            type: 'POST',
            success: function ()
            {
                $('#programDialog').modal('hide');
                loadPrograms();
            },
            error: function(jqXHR)
            {
                showErrors(jqXHR.responseJSON);
            }
        });
    });

});

function loadPrograms()
{
    $.ajax({
        url: 'programs',
        type: 'GET',
        dataType: 'json',
        success: function (progrms)
        {
            $('#programsTable tbody').html(null);

            if (progrms.length > 0)
            {
                $.each(progrms, function(){
                    var program = this;
                    $('#programsTable tbody').append(
                        $('<tr>').append(
                            $('<td>').text(this.name),
                            $('<td>').text(this.category.name),
                            $('<td>').text(this.description),
                            $('<td>').append(
                                $('<a class="clientIcons"><i class="glyphicon glyphicon-trash"></a>').on('click', function (event) {
                                    event.stopPropagation();
                                    deleteProgram(program.id);
                                })
                            )
                        ).on('click', function () {
                                location.assign('/programs/' + program.id + '/edit');
                        })
                    );
                });
            }
            else
            {
                $('#programsTable tbody').append(
                    $('<tr>').append(
                        $('<td colspan="2">').text('У вас пока нет тренировочных програм')
                    )
                );
            }
        }
    });
}

function addTrainingProgram()
{
    clearErrors();
    $('#programDialog').modal('show');
    loadCategories();
}


function loadCategories(then)
{
    $.ajax({
        url: '/categories',
        type: 'GET',
        dataType: 'json',
        success: function (cats)
        {
            $('select[name=category_id]').html(null);

            $.each(cats, function () {
                $('select[name=category_id]').append($('<option>').text(this.name).val(this.id));
            });

            if ($.isFunction(then))
            {
                then();
            }
        }
    });
}


function addWorkoutToProgram()
{
    $('#workoutTemplateDialog').modal('show');
}

function loadProgramWorkouts()
{
    $.ajax({
        url: '/programs/' + $('input[name=pID]').val(),
        dataType: 'json',
        success: function (program)
        {
            $('#programWorkouts').html(null);

            $.each(program.workouts, function () {

                var workoutTemplate = $('.workoutTemplate').clone(),
                    workoutId = this.id;
                workoutTemplate.removeClass('workoutTemplate');
                workoutTemplate.addClass('programWorkout');

                //Header & description

                workoutTemplate.find('.panel-body').append(
                    $('<h4>').text(this.name),
                    $('<p>').text(this.description)
                );

                //Exercises

                $.each(this.exercises, function () {

                    var exerciseTemplate = $('.workoutTemplateExercise').clone(),
                        nodeId = 'tex_' + workoutId + this.id;

                    exerciseTemplate.removeClass('workoutTemplateExercise');
                    exerciseTemplate.addClass('programWorkoutExercise');
                    exerciseTemplate.find('.exerciseHeading').attr('href', '#' + nodeId);
                    exerciseTemplate.find('.exerciseHeading').attr('aria-controls', nodeId);
                    exerciseTemplate.find('.exerciseHeading').text(this.name);
                    exerciseTemplate.find('#collapseExample').attr('id', nodeId);

                    $.each(this.metrics, function () {

                        var strategyHTML = this.strategies[0].enabled ? $('<small>').append(

                            '&nbsp;',

                            $('<i>').addClass(this.strategies[0].direction == 'up' ? 'fa fa-plus-circle' : 'fa fa-minus-circle'),

                            this.strategies[0].difference,

                            this.strategies[0].units == 'unit' ? this.unit.name : '%',

                            this.strategies[0].period == 'week' ? '/нед' : '/мес'

                        ) : '';

                        exerciseTemplate.find('.collapse').append(
                            $('<div>').text(this.name + ': ' + this.strategies[0].value).append(strategyHTML)
                        );
                    });

                    workoutTemplate.find('.panel-body').append(exerciseTemplate);

                });

                //Days of week in footer

                $.each(this.days, function () {
                    workoutTemplate.find('.panel-footer').append(
                        $('<span class="badge">').text(moment.weekdays(parseInt(this)))
                    );
                });

                //Edit & delete buttons in footer

                workoutTemplate.find('.panel-footer').append(
                    $('<div class="pull-right">').append(

                        $('<i class="workoutTemplateBtn glyphicon glyphicon-pencil">').on({
                            click: function()
                            {
                                editWorkoutTemplate(workoutId);
                            }
                        }),

                        $('<i class="workoutTemplateBtn glyphicon glyphicon-trash">').on({
                            click: function()
                            {
                                deleteWorkoutTemplate(workoutId);
                            }
                        })

                    )
                );

                $('#programWorkouts').append(workoutTemplate);
                workoutTemplate.fadeIn();
            })
        }
    });
}

function deleteProgram(id)
{
    if (confirm('Вы уверены?'))
    {
        $.ajax({
            url: '/programs/' + id,
            type: 'DELETE',
            success: function()
            {
                loadPrograms();
            }
        });
    }
}

$('.editable_edit_btn').on({
    click: function()
    {
        $(this)
            .parents('.editable_view')
            .hide();

        $(this)
            .parents('.editable_view')
            .next('.editable_controls')
            .show();
    }
});

$('.editable_save_btn').on({
    click: function ()
    {
        var btn = this;

        updateProgram(function()
        {
            location.reload();

            //$(btn)
            //    .parents('.editable_controls')
            //    .hide();
            //
            //$(btn)
            //    .parents('.editable_controls')
            //    .prev('.editable_view')
            //    .show();
        });
    }
});

function updateProgram(then)
{
    $.ajax({
        url: '/programs/' + $('input[name=pID]').val(),
        type: 'PUT',
        data:
        {
            name: $('input[name=p_name]').val(),
            category_id: $('select[name=category_id]').val(),
            description: $('input[name=p_description]').val()
        },
        success: function()
        {
            $.isFunction(then) ? then() : null;
        }
    });
}